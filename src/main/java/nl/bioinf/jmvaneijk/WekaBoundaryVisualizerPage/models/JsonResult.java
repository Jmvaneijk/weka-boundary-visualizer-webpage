package nl.bioinf.jmvaneijk.WekaBoundaryVisualizerPage.models;

import java.util.List;
import java.util.Objects;

/**
 * JsonResults bean that is created every 4 seconds and submitted to the front end.
 * The class contains the current status and list of ProcessedAlgorithms with the name and encodings.
 *
 * @author Jamie van Eijk
 */
public class JsonResult {
    String status;
    List<ProcessedAlgorithm> processedAlgorithms;

    /**
     * Constructor class receiving the status and list of processed algorithms.
     *
     * @param status contains the current status of the WekaBoundaryVisualizer, running or finished.
     * @param processedAlgorithms List containing the ProcessedAlgorithm objects with name and encoding.
     */
    public JsonResult(String status, List<ProcessedAlgorithm> processedAlgorithms){
        this.status = status;
        this.processedAlgorithms = processedAlgorithms;
    }

    /**
     * Simple getter for the status.
     *
     * @return The current status.
     */
    public String getStatus() {
        return status;
    }

    /**
     * Simple getter for the processedAlgorithms list.
     *
     * @return The list containing the ProcessedAlgorithm objects.
     */
    public List<ProcessedAlgorithm> getProcessedAlgorithms() {
        return processedAlgorithms;
    }

    @Override
    public String toString() {
        return "JsonResult{" +
                "status='" + status + '\'' +
                ", processedAlgorithms=" + processedAlgorithms +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof JsonResult)) return false;
        JsonResult that = (JsonResult) o;
        return Objects.equals(status, that.status) &&
                Objects.equals(processedAlgorithms, that.processedAlgorithms);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status, processedAlgorithms);
    }
}

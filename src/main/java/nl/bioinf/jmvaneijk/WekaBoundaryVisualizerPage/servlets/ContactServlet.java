package nl.bioinf.jmvaneijk.WekaBoundaryVisualizerPage.servlets;

import nl.bioinf.jmvaneijk.WekaBoundaryVisualizerPage.config.WebConfig;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet Responsible for serving the contact page with basic functionality.
 *
 * @author Jamie van Eijk
 */
@WebServlet(name = "ContactServlet", urlPatterns = "/contact")
public class ContactServlet extends HttpServlet {
    private TemplateEngine templateEngine;


    @Override
    public void init() throws ServletException {
        this.templateEngine = WebConfig.getTemplateEngine();
    }
    private static final long serialVersionUID = 1L;

    /**
     * Basic DoPost just loading the page.
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
        process(request, response);
    }

    /**
     * Basic DoGet just loading the page.
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        process(request, response);
    }

    /**
     * Process function to prevent repetitive code within Post and Get.
     */
    public void process(HttpServletRequest request, HttpServletResponse response) throws IOException {
        WebContext ctx = new WebContext(request, response, request.getServletContext(), request.getLocale());
        templateEngine.process("contact", ctx, response.getWriter());
    }
}
package nl.bioinf.jmvaneijk.WekaBoundaryVisualizerPage.models;

import java.util.Objects;

/**
 * The ProcessedAlgorithms class is a bean that stores the names of the algorithms
 * and the encodings of the images.
 *
 * @author Jamie van Eijk
 */
public class ProcessedAlgorithm {
    String algorithmName;
    String ImageEncoding;

    /**
     * Constructor class receiving the name of the algorithms and the encoding of the images.
     * @param algorithmName The name of the algorithm visualized in the image.
     * @param ImageEncoding Contains the encoded image of the visualization.
     */
    public ProcessedAlgorithm(String algorithmName, String ImageEncoding){
        this.algorithmName = algorithmName;
        this.ImageEncoding = ImageEncoding;
    }

    /**
     * Simple getter for the algorithm name.
     *
     * @return The name of the algorithm.
     */
    public String getAlgorithmName() {
        return algorithmName;
    }

    /**
     * Simple getter for the image encoding.
     *
     * @return The encoding of the image.
     */
    public String getImageEncoding() {
        return ImageEncoding;
    }

    @Override
    public String toString() {
        return "ProcessedAlgorithm{" +
                "algorithmName='" + algorithmName + '\'' +
                ", ImageEncoding='" + ImageEncoding + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProcessedAlgorithm)) return false;
        ProcessedAlgorithm that = (ProcessedAlgorithm) o;
        return Objects.equals(algorithmName, that.algorithmName) &&
                Objects.equals(ImageEncoding, that.ImageEncoding);
    }

    @Override
    public int hashCode() {
        return Objects.hash(algorithmName, ImageEncoding);
    }
}

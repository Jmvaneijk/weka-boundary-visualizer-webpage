package nl.bioinf.jmvaneijk.WekaBoundaryVisualizerPage.config;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * WebConfig creating TemplateEngine on startup which can be used to load HTML pages.
 * (https://michielnoback.github.io/java_gitbook/05_web/servlet_details.html)
 *
 * @author Michiel Noback
 */
@WebListener
public class WebConfig implements ServletContextListener {
    private static TemplateEngine templateEngine;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        System.out.println("[WebConfig] Initializing template engine");
        createTemplateEngine(servletContextEvent.getServletContext());
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        System.out.println("Shutting down!");
    }

    /**
     * Method called on startup creating the templateEngine.
     * @param servletContext servletContext Object
     */
    private static void createTemplateEngine(ServletContext servletContext) {
        ServletContextTemplateResolver templateResolver =
                new ServletContextTemplateResolver(servletContext);
        templateResolver.setTemplateMode("XHTML");
        templateResolver.setPrefix("/WEB-INF/templates/");
        templateResolver.setSuffix(".html");
        templateResolver.setCacheTTLMs(3600000L);
        templateResolver.setCacheable(true);

        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);
        WebConfig.templateEngine = templateEngine;
    }

    /**
     * When starting the application, the template engine will be served.
     * @return The templateEngine
     */
    public static TemplateEngine getTemplateEngine() {
        return templateEngine;
    }
}
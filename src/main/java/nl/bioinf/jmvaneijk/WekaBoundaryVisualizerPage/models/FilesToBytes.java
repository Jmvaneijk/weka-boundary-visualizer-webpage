package nl.bioinf.jmvaneijk.WekaBoundaryVisualizerPage.models;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

/**
 *  The FilesToBytes class can be utilized to convert .png files to image Encodings.
 *  These encodings can be used on HTML pages as img sources, or for other possible purposes.
 *
 * @author Jamie van Eijk
 */
public class FilesToBytes {
    String filepath;
    List<ProcessedAlgorithm> processedAlgorithms = new ArrayList<>();

    /**
     * Constructor class receiving the filepath.
     *
     * @param filepath contains the location of a folder containing .png files.
     */
    public FilesToBytes(String filepath){
        this.filepath = filepath;
    }

    /**
     * The fillProcessedAlgorithms class opens the submitted folder and gets all the files ending with .png.
     * These files are then encoded and this encoding alongside the filename
     * are stored in a list as processedAlgorithm objects.
     * When the encoding has been created, the original file will be deleted to save storage.
     *
     * @throws IOException If the files give issues.
     */
    private void fillProcessedAlgorithms() throws IOException {
        //Getting all the files
        File file = new File(filepath);
        File[] files = file.listFiles();

        // If folder not empty loop through files
        if(files != null){
            for (File f : files){

                // Filter all files ending on .png
                if(f.getName().endsWith(".png")){

                    // Splitting to get the filename and extension
                    String fileName = f.getName().split("\\.")[0];

                    // Using Base64 to encode the image from byte array to string
                    byte[] imageBytes = Files.readAllBytes(Path.of(filepath + f.getName()));
                    Base64.Encoder encoder = Base64.getEncoder();
                    String encoding = "data:image/png;base64," + encoder.encodeToString(imageBytes);

                    // Delete Original .png file
                    f.delete();

                    // Create ProcessedAlgorithm object and add to list
                    ProcessedAlgorithm processedAlgorithm = new ProcessedAlgorithm(fileName, encoding);
                    processedAlgorithms.add(processedAlgorithm);
                }
            }
        }
    }

    /**
     * Simple getter function that fills the list and returns it to the user.
     *
     * @return List containing the processedAlgorithm objects.
     * @throws IOException Because the fillProcessedAlgorithms class is called the Exception is also thrown here.
     */
    public List<ProcessedAlgorithm> getProcessedAlgorithms() throws IOException {
        this.fillProcessedAlgorithms();
        return processedAlgorithms;
    }
}

/**
 * This script takes the information from the contact form.
 * It then checks if everything was successful and starts building the email.
 * This mail is processed with XMLHTTPRequest and send using ajax.
 *
 * These mails are send using Formspree and they also provide you with the code upon registering an email:
 * https://formspree.io/
 *
 * @author Formspree / Jamie van Eijk
 */

window.addEventListener("DOMContentLoaded", function() {

    // Get the form elements defined in your form HTML above
    var form = document.getElementById("contactForm");
    var button = document.getElementById("contactFormBTN");
    var status = document.getElementById("contactFormStatus");

    /**
     * Check if submitting the form went okay.
     */
    function success() {
        form.reset();
        button.style = "display: none ";
        status.innerHTML = "Thank you!";
        $("#returnHome").fadeIn();
    }

    /**
     * Gives error message is something went wrong.
     */
    function error() {
        status.innerHTML = "Oops! There was a problem, please try again!";
    }

    // handle the form submission event
    form.addEventListener("submit", function(ev) {
        ev.preventDefault();
        var data = new FormData(form);
        ajax(form.method, form.action, data, success, error);
    });
});

/**
 * Using XMLHttpRequest and Ajax the email is send to me.
 *
 * @param method    method
 * @param url       url
 * @param data      data for email
 * @param success   succes status
 * @param error     error status
 */
function ajax(method, url, data, success, error) {
    var xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = function() {
        if (xhr.readyState !== XMLHttpRequest.DONE) return;
        if (xhr.status === 200) {
            success(xhr.response, xhr.responseType);
        } else {
            error(xhr.status, xhr.response, xhr.responseType);
        }
    };
    xhr.send(data);
}
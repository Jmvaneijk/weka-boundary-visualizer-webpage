package nl.bioinf.jmvaneijk.WekaBoundaryVisualizerPage.servlets;

import com.google.gson.Gson;
import nl.bioinf.jmvaneijk.WekaBoundaryVisualizerPage.models.FilesToBytes;
import nl.bioinf.jmvaneijk.WekaBoundaryVisualizerPage.models.JsonResult;
import nl.bioinf.jmvaneijk.WekaBoundaryVisualizerPage.models.ProcessedAlgorithm;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Servlet Responsible for checking the status of the visualization process and sending
 * the information back to the front-end once finished.
 *
 * @author Jamie van Eijk
 */
@WebServlet(name = "StatusServlet", urlPatterns = "/status")
public class StatusServlet extends HttpServlet {

    /**
     * doGet method checking for the current status every 4 seconds and sending information back to
     * the front-end.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Getting the session
        HttpSession session = request.getSession();
        String sessionId = session.getId();

        // Assign some useful objects for later
        final PrintWriter writer = response.getWriter();
        JsonResponse jsonResponse = new JsonResponse();

        // Select outputFolder
        String outputFolder = System.getProperty("java.io.tmpdir");
        outputFolder = outputFolder+"\\WekaBoundaryVisualizer\\"+sessionId+"\\";

        // Get error status
        String errorFileName = outputFolder+"error.txt";
        File errorFile = new File(errorFileName);
        boolean errorFileExist = errorFile.exists();

        // Get finished status
        String finishedFileName = outputFolder+"finished.txt";
        File finishedFile = new File(finishedFileName);
        boolean finishedFileExist = finishedFile.exists();

        // Send information
        FilesToBytes filesToBytes = new FilesToBytes(outputFolder);
        JsonResult jsonResult;

        if(errorFileExist){
            jsonResult = new JsonResult("error", null);

            errorFile.delete();
            File folder = new File(outputFolder);
            folder.delete();
        } else {
            if (finishedFileExist){
                // If file in directory send finished signal
                List<ProcessedAlgorithm> processedAlgorithms = filesToBytes.getProcessedAlgorithms();
                jsonResult = new JsonResult("finished", processedAlgorithms);

                // Deleting the finished.txt and folder
                finishedFile.delete();
                File folder = new File(outputFolder);
                folder.delete();

            } else {
                // If not, send loading signal
                jsonResult = new JsonResult("running", null);
            }
        }

        System.out.println("The current status is: " + jsonResult.getStatus());
        jsonResponse.responseObject = jsonResult;

        // Write info with Gson
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        Gson gson = new Gson();
        writer.write(gson.toJson(jsonResponse));
        writer.flush();
    }
}

/**
 * Simple storage class to submit to front-end with Json.
 */
class JsonResponse {
    String errorMessage = "NO ERRORS";
    String responseType = "boundaryVisualizer";
    Object responseObject;
}

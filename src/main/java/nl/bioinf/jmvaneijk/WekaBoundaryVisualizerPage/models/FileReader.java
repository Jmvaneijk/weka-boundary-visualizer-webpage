package nl.bioinf.jmvaneijk.WekaBoundaryVisualizerPage.models;

import weka.core.Instances;
import weka.core.converters.ArffLoader;
import weka.core.converters.CSVLoader;

import java.io.IOException;
import java.io.InputStream;

/**
 * The FileReader class is used to validate the type of the submitted file
 * and process the results into an Instance object for further usage.
 * This class also checks if the file contains 2D numeric data and responds accordingly.
 *
 * @author Jamie van Eijk
 */
public class FileReader {
    String filename;
    InputStream fileInput;

    /**
     * Constructor class receiving the filename and InputStream of a file.
     *
     * @param filename contains the name of the submitted file.
     * @param fileInput contains the file data as an InputStream.
     */
    public FileReader(String filename, InputStream fileInput){
        this.filename = filename;
        this.fileInput = fileInput;
    }

    /**
     * Using a switch/case, the type extension of a file is checked.
     * If the extension is not accepted, an IllegalArgumentException is called.
     * When a file with the correct extension is submitted, the processing functions for them are called.
     *
     * @return The Instances from the file.
     *
     * @throws IOException From Weka Boundary Visualizer.
     */
    public Instances checkFileType() throws IOException {
        String[] filenameSplit = filename.split("\\.");
        String extension = "."+filenameSplit[filenameSplit.length-1];

        return switch (extension) {
            case ".csv" -> readCSVFile();
            case ".arff" -> readARFFFile();
            default -> throw new IllegalArgumentException("File submitted with incorrect extension " + extension);
        };
    }

    /**
     * If the file has a .csv extension, this function will process it.
     *
     * @return The Instances from the .csv file.
     *
     * @throws IOException From Weka Boundary Visualizer
     */
    private Instances readCSVFile() throws IOException {
        CSVLoader csvLoader = new CSVLoader();
        csvLoader.setSource(fileInput);

        Instances instances = csvLoader.getDataSet();
        boolean validation = numericValidation(instances);
        if (validation){
            return instances;
        } else return null;
    }

    /**
     * If the file has a .arff extension, this function will process it.
     *
     * @return The Instances from the .arff file.
     *
     * @throws IOException From Weka Boundary Visualizer
     */
    private Instances readARFFFile() throws IOException {
        ArffLoader arffLoader = new ArffLoader();
        arffLoader.setSource(fileInput);

        Instances instances = arffLoader.getDataSet();
        boolean validation = numericValidation(instances);
        if (validation){
            return instances;
        } else return null;
    }

    /**
     * Because nominal data cannot properly be visualized, files without enough numeric data will be filtered out.
     * The method counts the amount of numeric instances and if it is bigger than 2, it will be validated.
     *
     * @param instances Instances object extracted from .csv or .arff file.
     *
     * @return Validation status.
     */
    private boolean numericValidation(Instances instances) {
        int numericCount = 0;

        String instancesString = instances.toString();
        String[] data = instancesString.split("\n");

        for(String line : data){
            if(line.startsWith("@attribute")){
                String[] lineContent = line.split(" ");
                if(lineContent[2].equals("numeric")){
                    numericCount += 1;
                }
            }
        }
        return numericCount == 2;
    }

}
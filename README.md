# Weka Boundary Visualizer Webpage README #

### Project Goals ###

Weka is a computer program that focuses on machine learning. Within Weka, you can execute dozens of classifiers,
apply filters, perform statistical tests and visualize data. This latter visualization step within Weka, called the 
BoundaryVisualizer, allows you to create images of 2D data showing how to algorithm decided to label each instance
using colors and the boundaries between them.

Because these visualizations were only available within the actual Weka software, I wanted to make it more accessible, 
while still showing the user that it is part of Weka. 

Since the website only requires you to tick a couple of checkboxes and submit a 2D file, it should be 
easy to use for anybody interested in visualizing their favourite algorithms.

The current version of the project is 1.0. There are still many features that can be implemented over time, but for now
the basics are here!


### Getting Started ###

##### Quick Disclaimer #####
When you first clone this repo, you can simply open the folder with your favourite editor. Because I personally
use the InteliJ IDE, the tutorial steps will be show from this IDE. This could mean that the steps might be different
if you are using another IDE.

##### Getting your host #####

To build this project, TomCat was used for the hosting, and I would also recommend using it.
[TomCat](http://tomcat.apache.org/) can be downloaded from their website and should be installed within an accessible
directory on your computer. For more information on installing TomCat, please check the original documentation.

##### Building the project & Dependencies #####
The application requires a java version of 11 or higher, alongside some dependencies:

- [Servlets](https://tomcat.apache.org/tomcat-5.5-doc/servletapi/) version 3.1.0 is used to create servlets that
 load pages, execute code and link the front to the back end.

- [Thymeleaf](https://www.thymeleaf.org/) version 3.0.11.RELEASE is used for loops, templates,
 translation etc. within the HTML.

- [JUnit5](https://junit.org/junit5/docs/current/user-guide/) version 5.6.2 is used for testing within the code.

- [The Weka API](https://waikato.github.io/weka-wiki/use_weka_in_your_java_code/) version 3.8.0 
 is used to get the BoundaryVisualizer.
 
- [Gson (Google JSON)](https://github.com/google/gson) version 2.8.5 is used for sending JSON data from the servlets
 to the front-end javascript's.

- [HTTPClient](https://docs.microsoft.com/en-us/dotnet/api/system.net.http.httpclient?view=net-5.0) version 4.5.10 is
 used for sending HTTP requests and receiving HTTP responses from a resource identified by a URI.

These dependencies need to be installed within the build.gradle file and can be done as follows.
(If the build.gradle from this repository is used, the packages will automatically be installed when loading the file.)

```
dependencies {
    //Servlets
    implementation 'javax.servlet:javax.servlet-api:3.1.0'
    //Thymeleaf
    implementation 'org.thymeleaf:thymeleaf:3.0.11.RELEASE'
    //JUnit5
    testImplementation 'org.junit.jupiter:junit-jupiter-api:5.7.0'
    testRuntimeOnly 'org.junit.jupiter:junit-jupiter-engine:5.7.0'
    //Weka
    implementation(group: 'nz.ac.waikato.cms.weka', name: 'weka-stable', version: '3.8.0')
    //Gson
    compile 'com.google.code.gson:gson:2.8.5'
    // HTTPClient
    compile group: 'org.apache.httpcomponents', name: 'httpclient', version: '4.5.10'
```

If you would like to use another build tool, you can simply take these links and information to implement it for yourself.

##### Running the website #####

With TomCat somewhere installed on your computer, and the required dependencies loaded, it is time to set up the 
configurations and run the website.

The first step in this process is configuring the installed TomCat host as a webserver within InteliJ. You can do
this by going to Settings -> Build, Execution, Deployment -> Application Servers and clicking on the + (add) icon.
Within the pop-up opened by clicking the + you should select the TomCat server and select the "apache-tomcat-<VERSION>" 
file. 

![Webserver Config](ReadmeImages/ConfigureWebserver.PNG)

The next step is creating the actual InteliJ run configuration. If you have no configuration added yet, there will be
an option to "Add Configuration..." at the top-right. Otherwise, you can just add another configuration from there. 

Within this window click on the + (add) in the top-left and find/select TomCat Server Local. 

![RunDebug Config1](ReadmeImages/RunDebug_Config1.png)

This will open a form containing some fields which need to be adjusted. These fields should be filled as shown
in the following screenshot.

![RunDebug Config2](ReadmeImages/RunDebug_Config2.png)

Before clicking on apply and starting the site, you can see "Fix" in the bottom right of the window. Click this
button and select the WekaBoundaryVisualizerPage version 1.0.0 .war file, not the exploded one. Then remove everything
from the Application Context field at the bottom except the "/" and hit apply.

![RunDebug Config3](ReadmeImages/RunDebug_Config3.png)

Now you are able to start your project by clicking the green play icon in the top-right!

### Use-Case Demo ###

For more information on the website itself or a quick and simple demonstration please watch [this video!](https://www.youtube.com/watch?v=SEEdN3eVLIc&feature=youtu.be&ab_channel=JamieVanEijk)

### Contribution guidelines ###

Anybody who would be interested in doing code reviews is welcome to do so. All the help that I can get in improving
this website will be greatly appreciated.

### Weka Software ###

In case you would like to know more about the actual Weka software, or try it for yourself, you can find the
page right here:

[Weka](https://www.cs.waikato.ac.nz/ml/weka/)

### Contact ###

If there are any issues regarding the website or questions that you might have, Please contact my at:
Ja.m.van.eijk@st.hanze.nl

I will do my best to get back at you as soon as possible!
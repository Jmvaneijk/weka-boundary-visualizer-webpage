/**
 * This script takes the information from the checkboxes and file-input.
 * These are then stored in a FormData object and send to the POST using XHLHttpRequest.
 * After the request has been send, the interval is started which requests the status every 4 seconds.
 * When the status is finished, the loader stops and the results are posted.
 * The Script also manages errors. When these errors occur the page is reloaded and the user will be notified why.
 *
 * @type {FormData} Contains the checkbox info and file.
 *
 * @author Jamie van Eijk
 */

/**
 * JQuery function verifying that a checkbox and file have been selected before allowing the user to submit.
 * After the form contains the correct amount of data, it will be validated and the visualization process will begin.
 */
$(function() {
    $("form[name='UploadForm']").validate({
        rules: {
            algorithms: {
                required: true,
                minlength: 1
            },
            MyFile: "required"
        },
        messages: {
            algorithms: "Please select at least one checkbox",
            MyFile: "Please submit a file",
            visualizerTimer: "Please set a timer value higher than or equal to 15"

        },
        errorLabelContainer: ".js-errors",
        errorElement: "li",
        submitHandler: function(form) {
            var formData = new FormData();
            submit(formData); showResults();
        }
    });
});


/**
 * The submit function adds the checkbox info and file to the formData and sends this
 * to the POST of the /run servlet for further processing.
 */
function submit(formData) {
    // Getting checkboxes
    let algorithms = $("input:checkbox:checked").map(function(){
        return this.value;
    }).toArray();

    // Filling formData
    formData.append("algorithms", algorithms);

    formData.append("timer", document.getElementById("visualizerTimer").value)

    formData.append("MyFile", document.getElementById("MyFile").files[0]);

    // Submission to POST
    var xhr = new XMLHttpRequest;
    xhr.open('POST', '/run', true);
    xhr.send(formData);
}

/**
 * This function starts after the data has been submitted and hides the original form.
 * Using an interval the /status servlet in pinged every 4 seconds for the current status (running/finished).
 * Once the status is finished, the loader and interval are both stopped and the results are posted.
 */
function showResults() {
    // Hiding form and starting loading screen
    $("#VisualizerPage").fadeOut(400)
    $("#VisualizationResults").fadeIn(400)

    /**
     * Inner function of showResults that processes the JSON request
     * and responsible for creating the HTML content.
     */
    function getStatus() {

        console.log("Fetching status from servlet")

        // Getting the info through JSON
        $.getJSON("/status", function (data){

            console.log("processing" + JSON.stringify(data))

            let boundaryVisualizer = data.responseObject;

            console.log("Status is currently: " + boundaryVisualizer.status);

            // If an error occurs, the page is reloaded and the error div is shown
            if (boundaryVisualizer.status === "error"){
                console.log("error found!")

                // Fade out loader
                $("#VisualizationResults").fadeOut(400)

                // Reset the current information
                $("#UploadForm").trigger("reset")
                fileChosen.textContent = "No file chosen"

                // Show error header and form for re-submission
                $("#errorHeader").fadeIn(400)
                $("#VisualizerPage").fadeIn(400)

                window.clearInterval(VisualizerInterval);
            }

            // If the finished file exists and the finished signal is send, fade out loader and post results
            if (boundaryVisualizer.status === "finished"){
                console.log("File creation process is finished! Stopping interval!")
                var image;
                var imagesHTML = "";
                imagesHTML += '<div class="grid-x grid-margin-x small-up-2 medium-up-4 large-up-6">\n'

                for (image of boundaryVisualizer.processedAlgorithms) {
                    imagesHTML += '<div class="cell">'
                    imagesHTML += '<div class="demo">'
                    imagesHTML += '<img src='+image.ImageEncoding + ' alt='+image.algorithmName +
                        ' width="400" height="400">'
                    imagesHTML += '<h3>'+image.algorithmName+'</h3>'
                    imagesHTML += '<a href='+image.ImageEncoding + ' download='+image.algorithmName+'>'
                    imagesHTML += '<button type="button" class="button rounded bordered shadow secondary">Download' +
                        '</button></a>'
                    imagesHTML += '</div>'
                    imagesHTML += '</div>'
                }
                imagesHTML += '</div>'

                $("#Images").html(imagesHTML);

                // Stopping the loading screen and interval
                $("#runner").fadeOut(400)
                $("#Finished_visuals").fadeIn(400)
                window.clearInterval(VisualizerInterval);
            }
        })
    }

    // Check for status every 4 seconds
    let VisualizerInterval = window.setInterval(function(){
        console.log("Sending Ping!")
        getStatus();}, 4000);
}

/*
After a file has been selected display the filename on screen.
 */
const actualBtn = document.getElementById('MyFile');

const fileChosen = document.getElementById('file-chosen');

actualBtn.addEventListener('change', function(){
    fileChosen.textContent = "Selected: " + this.files[0].name
})


package nl.bioinf.jmvaneijk.WekaBoundaryVisualizerPage.servlets;

import nl.bioinf.jmvaneijk.WekaBoundaryVisualizerPage.models.FileReader;
import nl.bioinf.jmvaneijk.WekaBoundaryVisualizerPage.models.WekaBoundaryVisualizer;
import weka.core.Instances;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 * Servlet Responsible for receiving the submitted information and starting the visualization process.
 *
 * @author Jamie van Eijk
 */
@WebServlet(name = "RunServlet", urlPatterns = "/run")
@MultipartConfig
public class RunServlet extends HttpServlet {

    /**
     * DoPost method collecting the submitted information, printing a summary of this info,
     * creating the output and starting the visualization process.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Getting the session information
        HttpSession session = request.getSession();
        String sessionID = session.getId();

        // Receiving the algorithms
        String[] algorithms = request.getParameterValues("algorithms");
        algorithms = algorithms[0].split(",");

        // Receiving the timer
        int timer = Integer.parseInt(request.getParameter("timer"));

        // Receiving the file
        Part filePart = request.getPart("MyFile");
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();

        // Receiving the output location
        String outputFolder = System.getProperty("java.io.tmpdir");
        outputFolder = outputFolder+"\\WekaBoundaryVisualizer\\"+sessionID+"\\";
        Files.createDirectories(Paths.get(outputFolder));

        // Printing short summary of received information
        System.out.println("----------------------------------------------------------------------");
        System.out.println("Submitted Info:");
        System.out.println("Algorithms: " + Arrays.toString(algorithms));
        System.out.println("Timer: " + timer);
        System.out.println("File: " + fileName);
        System.out.println("Output Location: " + outputFolder);
        System.out.println("----------------------------------------------------------------------");

        // Collecting the actual file info and starting the visualization process
        InputStream fileContent = filePart.getInputStream();

        FileReader fileReader = new FileReader(fileName, fileContent);
        Instances fileReaderResults = fileReader.checkFileType();

        if (fileReaderResults == null) {
            try {
                File outfile = new File(outputFolder + "error.txt");
                if (outfile.createNewFile()) {
                    System.out.println("File created: " + outfile.getName());
                } else {
                    System.out.println("File already exists.");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        else {
            WekaBoundaryVisualizer wekaBoundaryVisualizer = new WekaBoundaryVisualizer((Instances) fileReaderResults,
                    algorithms, outputFolder, timer);
            try {
                wekaBoundaryVisualizer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

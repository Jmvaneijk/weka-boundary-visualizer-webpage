package nl.bioinf.jmvaneijk.WekaBoundaryVisualizerPage.models;

import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.SMO;
import weka.classifiers.lazy.IBk;
import weka.classifiers.rules.OneR;
import weka.classifiers.rules.ZeroR;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.RandomForest;
import weka.classifiers.trees.RandomTree;
import weka.core.Instances;
import weka.gui.boundaryvisualizer.BoundaryVisualizer;
import weka.gui.visualize.JComponentWriter;
import weka.gui.visualize.PNGWriter;
import weka.classifiers.Classifier;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * The WekaBoundaryVisualizer inherits the original BoundaryVisualizer class produced by the Weka team.
 * By utilizing multi-threading this class opens a visualizer window for each requested algorithm and
 * visualizes this algorithm on the submitted data, greatly reducing the waiting time.
 * The visualizations are saved in a temporary session specific folder and cropped.
 * When all the visualizations are finished or stopped, the finished.txt file is also created
 * signaling the front end to show the visualizations.
 *
 * @author Jamie van Eijk
 */
public class WekaBoundaryVisualizer extends BoundaryVisualizer{
    // Adding variables to this
    Instances instances;
    String[] algorithms;
    String outputFolder;
    int timer;

    // Creating objects for each possible algorithm
    final Map<String, Classifier> algorithmMap = new HashMap<>() {{
        put("ZeroR", new ZeroR());
        put("OneR", new OneR());
        put("J48", new J48());
        put("RandomForrest", new RandomForest());
        put("RandomTree", new RandomTree());
        put("NaiveBayes", new NaiveBayes());
        put("NearestNeighbor", new IBk());
        put("SMO", new SMO());
    }};

    /**
     * Constructor class receiving the instances, requested algorithms and output folder.
     *
     * @param instances Instance object acquired from the submitted file.
     * @param algorithms Array with the requested algorithms from the checkboxes.
     * @param outputFolder Session specific output folder for the images and finished.txt.
     * @param timer Amount of seconds the boundary visualizer should run.
     */
    public WekaBoundaryVisualizer(Instances instances, String[] algorithms, String outputFolder, int timer){
        this.instances = instances;
        this.algorithms = algorithms;
        this.outputFolder = outputFolder;
        this.timer = timer;
    }

    /**
     * The start function created a thread for each requested algorithm.
     * During this thread, the images are visualized and cropped.
     * After the timer runs out, the threads will be stopped and the finished file is created.
     *
     * @throws InterruptedException When an interruption to the thread occurs this Exception is called.
     * @throws IOException Since an outputfile is created, the fileIO exception is also here.
     */
    public void start() throws InterruptedException, IOException {
        // Creating multi-thread service
        ExecutorService es = Executors.newCachedThreadPool();

        // Creating thread for each algorithm and running the code
        for(String algorithm : algorithms){
            es.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        visualizeBoundaries(algorithm);
                    } catch (Exception e) {
                        System.err.println("####ERROR FOUND####");
                        System.exit(0);
                    }
                }
            });
        }

        // When the processes are finished, the finished.txt is created
        es.shutdown();
        boolean finished = es.awaitTermination(timer, TimeUnit.SECONDS);
        if(finished){
            System.out.println("all task finished!");
            try {
                File outfile = new File(outputFolder+"finished.txt");
                if (outfile.createNewFile()) {
                    System.out.println("File created: " + outfile.getName());
                } else {
                    System.out.println("File already exists.");
                }
            } catch (IOException e) {
                System.err.println("####ERROR FOUND####");
                System.exit(0);
            }
        }
    }

    /**
     * Simple method calling other methods used within the multi-threading process.
     *
     * @param algorithmName Takes the name of the algorithms to get the object and for the final file name.
     * @throws Exception Throws exception in case anything goes wrong.
     */
    private void visualizeBoundaries(String algorithmName) throws Exception {
        // Creating a JFrame that will contain the visualizations
        final JFrame jf = new JFrame(
                "Weka classification boundary visualizer");

        // Creating the panel with visualization
        BoundaryVisualizer bvPanel = this.createVisualizerPanel(algorithmMap.get(algorithmName), jf);

        // Saving the panel as image
        this.savePanelAsIMG(bvPanel, algorithmName);

        // Killing the window
        jf.dispose();

        // Cropping the image to contain the plot
        this.cropImage(algorithmName);
    }

    /**
     * This method creates the main panel in which the classifier will be visualized on the requested instances.
     *
     * @param classifier Requested classifier algorithm.
     *
     * @return A BoundaryVisualizer window containing the image that should be saves.
     * @throws Exception Throws exception in case anything goes wrong.
     */
    private BoundaryVisualizer createVisualizerPanel(Classifier classifier, JFrame jf) throws Exception {
        WekaBoundaryVisualizer wekaBoundaryVisualizer = new WekaBoundaryVisualizer(instances, algorithms,
                                                                                    outputFolder, timer);

        // Set the properties of the window
        jf.getContentPane().setLayout(new BorderLayout());
        jf.getContentPane().add(wekaBoundaryVisualizer, BorderLayout.CENTER);
        jf.setSize(wekaBoundaryVisualizer.getMinimumSize());

        jf.pack();

        // Making the window invisible, so it can silently be deleted later
        jf.setVisible(false);

        // Setting the classifier and data requested
        wekaBoundaryVisualizer.setClassifier(classifier);
        wekaBoundaryVisualizer.setInstances(instances);
        wekaBoundaryVisualizer.m_plotTrainingData.doClick();
        wekaBoundaryVisualizer.m_startBut.doClick();
        System.out.println("going to sleep zzzz");

        // Allow the window to visualize for 30 seconds
        TimeUnit.SECONDS.sleep(timer-10);
        return wekaBoundaryVisualizer;
    }

    /**
     * The window containing the visualization will now be "screenshotted" and saved in the temp folder.
     *
     * @param bvPanel The previously created panel containing the visualization.
     * @param name The name of the algorithm used as filename.
     * @throws Exception Throws exception in case anything goes wrong.
     */
    private void savePanelAsIMG(BoundaryVisualizer bvPanel, String name) throws Exception {
        // Creating the file if it does not exist yet
        try {
            File outfile = new File(outputFolder+name+".png");
            if (outfile.createNewFile()) {
                System.out.println("File created: " + outfile.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.err.println("####ERROR FOUND####");
            System.exit(0);
        }

        // Using the weka PNGWriter to create an image of the window, which needs to be cropped
        PNGWriter writer = new PNGWriter();
        File outfile = new File(outputFolder+name+".png");
        JComponentWriter.toOutput(writer, bvPanel , outfile);
    }

    /**
     * Once the image is created, it should be cropped to only show the visualization.
     *
     * @param name The name of the output file.
     * @throws IOException Throws IOException since the code works with FileIO.
     */
    private void cropImage(String name) throws IOException {
        // UPDATED INTO MORE EFFICIENT STORAGE/STREAM LATER

        // Loading in the image
        File imageFile = new File(outputFolder+name+".png");
        BufferedImage bufferedImage = ImageIO.read(imageFile);

        // Cropping the image
        BufferedImage croppedImage = bufferedImage.getSubimage(0, 253, 440, 408);

        // Saving the cropped image under the name
        File pathFile = new File(outputFolder+name+".png");
        ImageIO.write(croppedImage,"jpg", pathFile);
    }
}